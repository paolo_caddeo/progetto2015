/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unica.pr2.progetto2015.g43779;

import it.unica.pr2.progetto2015.interfacce.SheetFunction;

/**
 * Reimplementazione della funzione di LibreOffice ARROTONDA.DIFETTO
 * @author Caddeo Paolo
 */
public class Semplice implements SheetFunction {

    /**
     * Il metodo che esegue effettivamente la funzione, dati un numero e una base
     * arrotonda per difetto il numero al multiplo di base più vicino
     * @param args Il numero, la base e il modo con cui arrotondare i numeri
     * negatitivi(opzionale), base e numero devono avere lo stesso segno
     * @return Il numero approssimato
     */
    @Override
    public Object execute(Object... args) {
        
        //Verifica dei dati in input
        if (args.length == 0)
            return "###";
        
        if (args.length < 2)
            return "Err:511";

        if (args.length > 3)
            return "Err:504";

        double numero;
        double base;
        double modo = 0;

        //Inserimento dei dati in apposite variabili
        try {
            numero = ((Number)args[0]).doubleValue();
            base = ((Number)args[1]).doubleValue();
            if (args.length == 3)
                modo = ((Number)args[2]).doubleValue();
        } catch(ClassCastException e) {
            return "#VALORE!";
        }

        //if ((numero == null) || (base == null) || (modo == null))
        //    return "#VALORE!";

        //Se il numero o la base sono uguali a zero allora lo è anche il risultato
        if ((numero == 0) || (base == 0) || Math.abs(base) > Math.abs(numero))
            return 0;

        //verifica dei segni e calcolo dell'arrotondamento
        if (numero * base > 0) {
            double out = numero / base;
            
            //verifica errori di arrotondamento nella divisione
            if (Math.round(out) - out > 1E-10)
                out = Math.floor(out) * base;
            else
                out = Math.round(out) * base;
            
            if (numero < 0 && modo == 0.0) {
                if (Math.abs(out - numero) < 1E-10)
                    return numero;
                return out + base;
            }
            
            return out;
        }
        else
            return "Err:502";
    }

    /**
     * Restituisce la categoria LibreOffice;
     * Vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
     * ad esempio, si puo' restituire "Data&Orario" oppure "Foglio elettronico"
     */
    @Override
    public String getCategory() {
        return "Matematica";
    }

    /** 
     * Informazioni di aiuto
     */
    @Override
    public String getHelp() {
        return "Arrotonda per difetto al multiplo più prossimo a peso";
    }

    /** 
     * Nome della funzione.
     * vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
     * ad es. "VAL.DISPARI" 
     */ 
    @Override
    public String getName() {
        return "ARROTONDA.DIFETTO";
    }
}
