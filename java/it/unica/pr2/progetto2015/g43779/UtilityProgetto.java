/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unica.pr2.progetto2015.g43779;

import com.qqwing.Difficulty;
import com.qqwing.Symmetry;

/**
 * Insieme di costanti e funzioni statiche necessarie alla classe Custom del progetto 2015
 * @author Caddeo Paolo
 */
public class UtilityProgetto {
    public static final int NUM_COLONNE = 9;
    public static final int NUM_RIGHE = 9;
    public static final int NUM_CELLE = NUM_COLONNE * NUM_RIGHE;
    
    /**
     * Converte una matrice di Integer in un array di int (inversa di convertiInMatrice).
     * La conversione viene effettuata per riga
     * @param matrice La matrice da convertire
     * @return L'array equivalente alla matrice
     */
    public static int[] convertiInArray(Integer[][] matrice) {
        int[] array = new int[NUM_CELLE];
        int k = 0;
        for (int i = 0 ; i < NUM_RIGHE; i++) {
            for (int j = 0; j < NUM_COLONNE; j++) {
                array[k] = matrice[i][j];
                k++;
            }
        }
        return array;
    }
    
    /**
     * Converte un array di int in una matrice di Integer (inversa di convertiInArray).
     * La conversione viene ffettuata per riga
     * @param array
     * @return 
     */
    public static Integer[][] convertiInMatrice(int[] array) {
        Integer[][] matrice = new Integer[NUM_RIGHE][NUM_COLONNE];
        int k = 0;
        for (int i = 0; i < NUM_RIGHE; i++) {
            for (int j = 0; j < NUM_COLONNE; j++) {
                matrice[i][j] = array[k];
                k++;
            }
        }
        return matrice;
    }
    
    /**
     * Covnerte una stringa contenente la difficoltà (in italiano) e restituisce
     * il rispettivo enum Difficulty (inversa di diffToString)
     * @param diff La stringa con la difficoltà
     * @return L'enum di tipo Difficulty
     */
    public static Difficulty stringToDiff(String diff) {
        switch (diff.toLowerCase()) {
            case "semplice":
                return Difficulty.SIMPLE;
            case "facile":
                return Difficulty.EASY;
            case "intermedio":
                return Difficulty.INTERMEDIATE;
            case "esperto":
                return Difficulty.EXPERT;
            case "sconosciuta":
                return Difficulty.UNKNOWN;
        }

        return null;
    }
    
    /**
     * Converte un enum di tipo Difficulty in una Stringa(in italiano, inversa
     * di stringToDiff)
     * @param diff L'enum di tipo Difficulty
     * @return La Stringa corrispondente
     */
    public static String diffToString(Difficulty diff) {
        switch (diff) {
            case SIMPLE:
                return "Semplice";
            case EASY:
                return "Facile";
            case INTERMEDIATE:
                return "Intermedio";
            case EXPERT:
                return "Esperto";
            case UNKNOWN:
                return "Sconosciuta";
        }
        
        return null;
    }
    
    /**
     * Covnerte una stringa contenente la simmetria (in italiano) e restituisce
     * il rispettivo enum Symmetry (inversa di diffToString)
     * @param symm La stringa con la simmetria
     * @return L'enum di tipo Difficulty
     */
    public static Symmetry stringToSymm(String symm) {
        switch (symm.toLowerCase()) {
            case "rovesciato":
                return Symmetry.FLIP;
            case "specchiato":
                    return Symmetry.MIRROR;
            case "nessuna":
                return Symmetry.NONE;
            case "casuale":
                return Symmetry.RANDOM;
            case "ruotato180":
                return Symmetry.ROTATE180;
            case "ruotato90":
                return Symmetry.ROTATE90;
        }
        
        return null;
    }
}
