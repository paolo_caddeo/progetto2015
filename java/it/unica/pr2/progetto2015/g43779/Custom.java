/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unica.pr2.progetto2015.g43779;

import it.unica.pr2.progetto2015.interfacce.SheetFunction;
import com.qqwing.*;

/**
 * Funzione Custom del progetto 2015, genera un puzzle sudoku o, una volta impostato
 * un puzzle, ne ricava varie informazioni
 * @author Caddeo Paolo
 */
public class Custom implements SheetFunction {
    private static int VAL_MEDIA = 100;

    private QQWing sudoku;
    private int[] basePuzzle;
    private int[] statoPuzzle;
    private int[] soluzione;
    private boolean risolvibile = true;
    private double mediaTentativi;
    private int numAlternative;
    private Difficulty difficolta;
    
    public Custom() {
        basePuzzle = null;
        statoPuzzle = null;
        soluzione = null;
        risolvibile = false;
    }

    /**
     * Metodo principale della classe, svolge varie operazioni su dei puzzle sudoku e ne
     * restituisce il risultato
     * @param args Il primo parametro è il comando da eseguire e i restanti (se necessari) quelli necessari per il comnado
     * @return In base al comando selezionato, una stringa con il risultato, o una matrice di interi coontenente il puzzle
     */
    @Override
    public Object execute(Object... args) {
        QQWing tempPuzzle = new QQWing();
        tempPuzzle.setRecordHistory(true);
        String operazione;
        Difficulty diff = null;
        Symmetry symm = null;
        
        sudoku = new QQWing();
        sudoku.setRecordHistory(true);
        
        if (args.length == 0)
            return "###";
        
        try {
            operazione = ((String)args[0]).toLowerCase();
        } catch (ClassCastException e) {
            return "#VALORE!";
        }
        
        switch(operazione.toLowerCase()) {
            //Creazione di un nuovo Puzzle
            case "crea":
                //varifica della correttezza degli argomenti passati
                if (args.length > 3)
                    return "###";

                //Lettura della difficoltà e/o simmetria
                if (args.length > 1) {
                    try {
                        //lettura del primo parametro
                        diff = UtilityProgetto.stringToDiff((String)args[1]);
                        symm = UtilityProgetto.stringToSymm((String)args[1]);

                        //lettura del secondo parametro (se presente)
                        if (args.length == 3) {
                            if (diff == null)
                                diff = UtilityProgetto.stringToDiff((String)args[2]);
                            else
                                symm = UtilityProgetto.stringToSymm((String)args[2]);
                            
                            //controllo della presenza di un eventuale errore
                            //in uno o entrambi i parametri
                            if (diff == null || symm == null)
                                return "###";
                        }
                        //controllo di un eventuale errore quando si inserisce un solo
                        //parametro
                        if (diff == null && symm == null)
                            return "###";
                    }
                    catch (ClassCastException e) {
                        return "###";
                    }
                    if (diff == null && symm == null)
                        return "###";
                }

                creaPuzzle(diff,symm);
                return UtilityProgetto.convertiInMatrice(basePuzzle);
            
            //inserisce il sudoku su cui ricercare le informazioni
            case "imposta":
                
                //verifica della correttezza dei parametri
                if (args.length < 2)
                    return "Err:511";
                
                if (args.length > 2) 
                    return "Err:504";
                
                try {
                    statoPuzzle = UtilityProgetto.convertiInArray((Integer[][])args[1]);
                } catch (ClassCastException e) {
                    return "#VALORE!";
                }
                
                //aggiunge il puzzle solo se non contiene errori o se non è già
                //presente
                if (stessoPuzzle(statoPuzzle))
                    return true;
                
                //Inserisce il puzzle solo se presente
                if (stessoPuzzle(statoPuzzle))
                    return true;
                
                //e se non contiene errori
                if (sudoku.setPuzzle(statoPuzzle)) {
                    numAlternative = sudoku.countSolutionsLimited();
                    risolvibile = sudoku.solve();
                    basePuzzle = statoPuzzle.clone();
                    difficolta = sudoku.getDifficulty();
                    soluzione = sudoku.getSolution();
                    mediaTentativi = 0;
                    if (difficolta == Difficulty.EXPERT) {
                        for (int i = 0; i < VAL_MEDIA; i++) {
                            sudoku.solve();
                            mediaTentativi += sudoku.getGuessCount();
                        }
                    }
                    mediaTentativi /= VAL_MEDIA;
                        
                    return true;
                }
                
                return false;
                
            //Visualizza il puzzle impostato
            case "stampa":
                if (args.length > 1)
                    return "###";
                
                if (basePuzzle == null)
                    return 0;
                
                return UtilityProgetto.convertiInMatrice(basePuzzle);
                
            //Stampa un puzzle uguale a quello inserito in cui vengono
            //evidenziati gli errori (su libreoffice funziona interattivamente)
            case "mostraerrori":
                
                //verifica della correttezza dei parametri
                if (args.length < 2)
                    return "Err:511";
                
                if (args.length > 2) 
                    return "Err:504";
                
                if (!risolvibile)
                    return "0";
                
                //lettura del puzzle da correggere
                try {
                    statoPuzzle = UtilityProgetto.convertiInArray((Integer[][])args[1]);
                } catch (ClassCastException e) {
                    return "###";
                }
                
                //verifica che il puzzle da correggere sia uguale a quello inserito
                if (!stessoPuzzle(statoPuzzle))
                    return 0;
                
                //se il puzzle ha più di una soluzione lo si risolve a ogni modifica e,
                //se porta a una soluzione diversa da quella attuale la si aggiorna
                //per seguire il nuovo percorso
                if (!sudoku.hasUniqueSolution() && tempPuzzle.setPuzzle(statoPuzzle)) {
                    if (tempPuzzle.solve()) {
                        sudoku = tempPuzzle;
                        soluzione = sudoku.getSolution();
                        return UtilityProgetto.convertiInMatrice(statoPuzzle);
                    }
                }
                
                //ricerca di eventuali errori
                for (int i = 0; i < UtilityProgetto.NUM_CELLE; i++) {
                    if (basePuzzle[i] == 0) {
                        if (statoPuzzle[i] != soluzione[i])
                            statoPuzzle[i] *= -1;
                    }
                    //ripristina una cella della base del puzzle in caso sia stata
                    //modificata
                    else if (statoPuzzle[i] != basePuzzle[i])
                        statoPuzzle[i] = basePuzzle[i];
                }
                return UtilityProgetto.convertiInMatrice(statoPuzzle);
            
            //Stampa la soluzione del puzzle
            case "risolvi":
                if (args.length > 1)
                    return "###";
                
                if (!risolvibile)
                    return 0;
                
                return UtilityProgetto.convertiInMatrice(soluzione);
                
            //Stampa la difficoltà del puzzle
            case "difficolta":
                if (args.length > 1)
                    return "Err:504";
                
                if (!risolvibile)
                    return "Irrisolvibile";
                
                return difficolta;
                
            //Restituisce vero se il puzzle ha più di una soluzione, falso altrimenti
            case "variesoluzioni":
                if (args.length > 2)
                    return "Err:504";
                
                if (numAlternative == 2)
                    return true;
                else
                    return false;
                
            //Restituisce il numero di volte in cui bisogna tirare a indovinare per
            //risolvere il puzzle
            case "mediatentativi":
                if (args.length > 2)
                    return "Err:504";
                
                if (!risolvibile)
                    return -1;
                
                return mediaTentativi;
                
            default:
                return "#VALORE!";
        }
    }

    /**
     * Restituisce la categoria LibreOffice;
     * Vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
     * ad esempio, si puo' restituire "Data&Orario" oppure "Foglio elettronico"
     */
    @Override
    public String getCategory() {
        return "add-in";
    }

    /** 
     * Informazioni di aiuto
     */
    @Override
    public String getHelp() {
        return "Genera un nuovo puzzle o risolve/restituisce informazioni su quello selezionato";
    }

    /** 
     * Nome della funzione.
     * vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
     * ad es. "VAL.DISPARI" 
     */ 
    @Override
    public String getName() {
        return "SUDOKU";
    }
    
    /**
     * L'elenco delle opzioni selezionabili nella execute
     * @return L'array con le opzioni
     */
    public String[] getOpzioni() {
        return new String[] {"Crea", "Imposta", "Stampa", "MostraErrori", "Risolvi", "Difficolta", "VarieSoluzioni", "MediaTentativi"};
    }
    
    /**
     * L'elenco delle difficoltà disponibili
     * @return L'array con le difficoltà
     */
    public String[] getDifficolta() {
        return new String[] {"Semplice", "Facile", "Intermedio", "Esperto", "Sconosciuta"};
    }
    
    /**
     * L'elenco delle simmetrie disponibili
     * @return L'array con le simmetrie
     */
    public String[] getSimmetria() {
        return new String[] {"Rovesciato", "Specchiato", "Nessuna", "Casuale", "Ruotato180", "Ruotato90"};
    }
    
    
    /**
     * Genera un nuovo puzzle sudoku data la difficoltà e la simmetria desiderate
     * @param diff La difficoltà del puzzle da generare (null o UNKNOWN se non si
     * è interassati)
     * @param symm La simmetria del puzzle da generare (null o NONE se non si è 
     * interessati)
     */
    public void creaPuzzle(Difficulty diff, Symmetry symm) {
        if (diff == null)
            diff = Difficulty.UNKNOWN;
        
        if (symm == null)
            symm = Symmetry.NONE;

        if (diff != Difficulty.UNKNOWN) {
            do {
                sudoku.generatePuzzleSymmetry(symm);
                risolvibile = sudoku.solve();
            }
            while (sudoku.getDifficulty() != diff);
        }
        else {
            sudoku.generatePuzzleSymmetry(symm);
            risolvibile = sudoku.solve();
        }
        numAlternative = sudoku.countSolutionsLimited();
        sudoku.solve();
        difficolta = sudoku.getDifficulty();
        mediaTentativi = 0;
        if (difficolta == Difficulty.EXPERT) {
            for (int i = 0; i < VAL_MEDIA; i++) {
                sudoku.solve();
                mediaTentativi += sudoku.getGuessCount();
            }
        }
        mediaTentativi /= VAL_MEDIA;
        basePuzzle = sudoku.getPuzzle();
        soluzione = sudoku.getSolution();
    }
    
    /**
     * Verifica che il puzzle inserito non sia già presente
     * @param nuovoPuzzle Il puzzle su cui effettuare il controllo
     * @return true se è già presente, false altrimenti
     */
    public boolean stessoPuzzle(int[] nuovoPuzzle) {
        if (basePuzzle == null)
            return false;
        
        for (int i = 0; i < basePuzzle.length; i++) {
            if (basePuzzle[i] != 0 && basePuzzle[i] != nuovoPuzzle[i])
                return false;
        }
        return true;
    }
}
