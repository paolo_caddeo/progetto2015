/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unica.pr2.progetto2015.g43779;

import it.unica.pr2.progetto2015.interfacce.SheetFunction;

/**
 * Reimplementazione della funzione di LibreOffice MATR.SOMMA.PRODOTTO
 * @author Caddeo Paolo
 */
public class Complessa implements SheetFunction {
    
    /**
     * Il metodo che esegue effettivamente la funzione, date n matrice moltiplica
     * gli elementi nella stessa posizione di ogni matrice e somma il risultato
     * con gli altri prodotti
     * @param args Le matrici su cui eseguire la funzione (da 1 a un max di 30)
     * @return Un double risultante dalla funzione
     */
    @Override
    public Object execute(Object... args) {
        Number[][][] matrici;
        
        if (args.length == 0)
            return "Err:511";
        
        //Verifica che i dati in input siano validi e estrazione in una matrice
        //usata per l'elaborazione
        try {
            if (args.getClass() == Object[].class) {
                if (args.length > 30)
                    return "Err:504";
                        
                if ( !verificaInput(args))
                    return "#VALORE!";
                matrici = new Number[args.length][][];

                for (int i = 0; i < args.length; i++)
                    matrici[i] = (Number[][])args[i];
            }
            else {
                if ( !(args instanceof Object[][]))
                    return "#VALORE!";

                matrici = new Number[][][] {(Number[][])args};
            }
        } catch(ClassCastException e) {
            return "#VALORE!";
        }
        
        //Calcolo effettivo del risultato
        double somma = 0;
        for (int j = 0; j < matrici[0].length; j++) {
            for (int k = 0; k < matrici[0][0].length; k++) {
                double prodotto = 1;
                for (int i = 0; i < matrici.length; i++){
                    prodotto *= matrici[i][j][k].doubleValue();
                }
                somma += prodotto;
            }
        }
        
        return somma;
    }

    /**
     * Restituisce la categoria LibreOffice;
     * Vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
     * ad esempio, si puo' restituire "Data&Orario" oppure "Foglio elettronico"
     */
    @Override
    public String getCategory() {
        return "Matrice";
    }

    /** 
     * Informazioni di aiuto
     */
    @Override
    public String getHelp() {
        return "Restituisce la somma dei prodotti degli argomenti delle matrici (prodotto scalare)";
    }

    /** 
     * Nome della funzione.
     * vedere: https://help.libreoffice.org/Calc/Functions_by_Category/it
     * ad es. "VAL.DISPARI" 
     */   
    @Override
    public String getName() {
        return "MATR.SOMMA.PRODOTTO";
    }
    
    /**
     * Esegue un controllo sulle matrici in input per assicurarsi che abbiano
     * tutte la stessa dimensione
     * @param args I dati che vengono passati al metodo execute
     * @return true se tutte le matrici hanno la stessa dimensione, falso altrimenti
     */
    private static boolean verificaInput(Object[] args) {
        boolean dimUguale = true;
        Object[][] temp = (Object[][])args[0];
        int numRighe = temp.length;
        int numColonne = temp[0].length;

        for (Object o: args) {
            temp = (Object[][])o;
            if (temp.length != numRighe || temp[0].length != numColonne)
                dimUguale = false;
        }
        
        return dimUguale;
    }
}
