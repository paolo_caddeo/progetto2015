PROGETTO PR2 2015

Classi contenute:
	-Semplice
	-Complessa
	-Custom

Classe Semplice:
Reimplementazione della funzione di LibreOffice ARROTONDA.DIFETTO
La funzione prende in ingresso due numeri (interi o decimali),  numero e base, con lo stesso segno e restituisce il più grande multiplo di base minore di numero.
Se entrambi i numeri sono negativi si può aggiungere un terzo numero (modo) che, se diverso da zero fa restituire dalla funzione il mupltiplo più vicino a zero.

Classe Complessa:
Reimplementazione della funzione di LibreOffice MATR.SOMMA.PRODOTTO
La funzione prende in ingresso n matrici (1 <= n <= 30) e restituisce un numero che sarà dato dalla somma fra i prodotti degli elementi corrispondenti.

Classe Custom:
Implementazione della funzione SUDOKU
La funzione esegue varie operazioni riguardanti il gioco del sudoku, dalla creazione di un puzzle date difficoltà e simmetria, al reperimento di informazioni su un determinato puzzle. Per ottenere una lista delle opzioni,  difficoltà e simmetrie sono presenti, oltre ai metodi getHelp, getName e getCategory, altri tre metodi getOpzioni,  getDifficolta e getSimmetria, che restituiscono ciascuno un array di stringhe contenente la lista di possibilità.

Utilizzo:
Il primo parametro deve essere una stringa contenente il nome dell'operazione da eseguire mentre dal secondo in poi dipendono dall'operazione scelta

Opzioni:
	-Crea: genera un nuovo puzzle sudoku. (non è necessario inserire i parametri in ordine)
		-Input: String (opzionale),  la difficoltà del puzzle da generare
		        String (opzionale),  la simmetria del puzzle da generare
		-Output: Integer[][], il puzzle generato
	-Imposta: seleziona il puzzle da cui ricavare le informazioni (utilizzando 'crea' il puzzle creato viene impostato automaticamente).
		-Input: Integer[][],  il puzzle da impostare
		-Output: boolean,  conferma dell'inserimento
	-Stampa: stampa il puzzle presente.
		-Input:
		-Output: Integer[][], il puzzle presente
	-Risolvi: 
		-Input:
		-Output: Integer[][], la soluzione del puzzle impostato
	-Difficolta:
		-Input:
		-Output: String,  la difficoltà del puzzle impostato
	-VarieSoluzioni: restituisce un booleano che indica se il puzzle ha più di una soluzione possibile (i puzzle generati hanno sempre una sola soluzione).
		-Input:
		-Output: Boolean, true (1) se il puzzle impostato ha più di una soluzione, false (0) altrimenti
	-MediaTentativi: (solo per i puzzle con la difficoltà più alta) stampa la media delle volte in cui bisogna tirare a indovinare per risolvere il puzzle
		-Input:
		-Output: double, la media dei tentativi
	-MostraErrori: permette di visualizzare gli errori commessi mentre si esegue un puzzle
		-Input: Integer[][], il puzzle che si sta svolgendo
		-Output: Integer[][], il puzzle corrispondente a quello che si sta giocando con gli errori (se presenti) in evidenza
		*per utilizzare questa opzione sono necessarie 2 aree, dato che non è possibile fare tutto nella stessa area, una in cui si trova il puzzle che si sta svolgendo e una in cui viene visualizzato il puzzle con gli errori.
		*se si vuole utilizzare con un puzzle che è stato generato bisogna prima copiare i numeri nell'area in cui si deve giocare
		*ovviamente il puzzle deve essere stato impostato
		*per mettere meglio in evidenza gli errori è consigliabile impostare le celle in cui viene stamapata la correzione in modo che evidenzino i numeri negativi
	*in tutti i casi in cui si ha come input o output Integer[][], l'area selezionata deve essere 9*9